---
title: Sublime Unity
layout: post
---

Recently I've been playing around with Unity, but I quickly realized I wasn't a huge fan of MonoDevelop. I'd rather work in an environment I already felt comfortable in, so I decided to figure out how to set up Sublime Text to develop for Unity.

The instructions for how to do so are spread out across a couple different sites, so I thought I'd collect all the steps here at least for my own reference, but hopefully someone else finds it helpful too. These instructions have only been tested on OS X Yosemite.

**Step One.** Install [Sublime Text](http://www.sublimetext.com/). Step zero, really.  
**Step Two.** Install [Package Control](https://packagecontrol.io/). This makes installing packages and extensions for Sublime Text painless.  
**Step Three.** Use Package Control to install [Unity3D Syntax Highlighting](https://packagecontrol.io/packages/Unity3D).

This gets a very basic environment set up for using Sublime Text for Unity. However, you don't get nice things like autocompletion or compilation up front. The best way to get that set up is with [Omnisharp](http://www.omnisharp.net/), which has a few prerequisites.

**Step Four.** Install [Homebrew](http://brew.sh/).  I use Homebrew all the time as my package manager.  
**Step Five.** Use Homebrew to install [Mono](http://www.mono-project.com/) (`brew install mono`)  
**Step Six.** Use Package Control to install Omnisharp.  
**Step Seven.** Before you can actually use Omnisharp with a Unity project, you need to create a Sublime project file that identifies the solution file for Omnisharp. This is the standard template I've used.

    {
	    "folders":
	    [
		    {
			    "path": ".",
			    "file_exclude_patterns": ["*.meta"],
		    },
	    ],
	    "solution_file": "./NAME_OF_PROJECT.sln"
    }

I'm still toying with the right path to use/files to exclude here. I initially made the path "Assets/Scripts" because I only wanted to see the actual files I would be editing in Sublime. But I also wanted to be able to easily edit the project file in case I needed to make any tweaks.

At this point, you can easily use Sublime to edit any Unity scripts that you like. Each time you save the file the log should pop up at the bottom to show you any compilation errors or warnings. There's one last step to make it a little bit easier to integrate Sublime with Unity.

**Step Eight.** Set Sublime Text to be the default editor of Unity. Go to Preferences, select External Tools, and choose "Sublime Text" from the External Script Editor dropdown menu.

Now when you double-click a script in Unity, it should open in Sublime Text. For me, it's a much more comfortable environment than working in MonoDevelop.

**Update (2/6/2015)**: After playing with this a little more, I realized that you actually need to open the project in MonoDevelop at least once to generate the solution file and get the autocompletion enabled in Sublime. I'm still looking to see if there's a way to simplify it, but haven't figured it out yet.
