---
title: Coding Every Day for a Month
layout: post
---

I've been frustrated for a few years by the fact that I don't code as much as I'd like to. At Stanford I'm surrounded by people who are working on personal projects in their free time, but my personal project time has normally been consumed by section leading and mock trial. So a little over a month ago, I decided that I was going to set my sights modestly: I wanted to make one commit a day, every day. These are the results so far:

![A stats chart from Github of my activity over the last month]({% asset img/one_month_of_code.png %})

So far I've made:  

- 126 commits (this post will be the 127th)
- across 7 repos
- with 4129 additions and 2139 deletions
- in 6 different languages.

I don't necessarily think that coding every day is making me a better programmer. I'm working with a lot of new technologies I don't have experience with, and there's nobody guiding me through it, so I have no way of knowing if I'm doing the write thing, or if I'm doing it the right way. 

Another downside to coding every day is that it's difficult to tackle big problems. When I'm worried about getting a commit done for the day, I'm less likely to invest time in figuring out how to solve a big problem; I'll default to something that, though possibly still challenging, is something that I know I'll be able to manage. The most obvious (public) example of this is my [Shaping Up With Angular](https://github.com/abroder/shaping-up-with-angularjs/) project, based off of the [Code School class of the same name](http://campus.codeschool.com/courses/shaping-up-with-angular-js/intro), which stalled out once I realized I couldn't quite figure out how to make authentication work the way that I wanted it to.

Despite the downsides though, I'm still very happy that I took on the challenge. Sure, I might be struggling with the bigger problems, but before I rarely encountered *any* problems outside of my school work. My goal for the next month is to see if I can figure out a way to encourage spending more time on bigger problems, or reward myself for days that are primarily (or entirely) research-driven. Now that I have a decent streak going, the compulsion not to "break the chain" is strong.