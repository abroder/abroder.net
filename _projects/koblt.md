---
title: Koblt
website: http://koblt.com
languages: [C++, Unreal Engine, Javascript, Node.js]
---

Koblt is a platform to enable developers to create asymmetric virtual reality experiences. It acts as a bridge between a web-based application and 3D software developed in the Unreal Engine, allowing the VR space to be dynamically changed and modified on the fly. Some possible use cases we were particularly interested in include immersive training simulations and guided learning experiences.

I designed a lot of the architecture for the platform, and ended up implementing most of the Unreal Engine and server code.

Koblt was developed with advising from a representative from [Oculus](https://www.oculus.com/) as part of Stanford's [CS210](http://web.stanford.edu/class/cs210/) course. If you're interested in talking about the details of the implementation, please get in touch!