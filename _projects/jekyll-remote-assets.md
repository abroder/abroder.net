---
title: Jekyll Remote Assets
github: https://github.com/abroder/jekyll-remote-assets
languages: [Ruby]
---

Jekyll Remote Assets is an extension for Jekyll that allows you to host assets on Dropbox and automatically link to them on site generation. This can help save on hosting costs if you have a host that charges based on storage such as [NearlyFreeSpeech.NET](https://www.nearlyfreespeech.net/) like I do. 

Still in development, I started working on this project to learn about Jekyll plugins, OAuth, and get more practice working in Ruby. It's a little fragile now, but I hope to make it robust enough that it could be used by any Jekyll site.